import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {  browser, by, element } from 'protractor';
import { HomePage } from './home.page';
import { FormBuilder } from '@angular/forms';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let page: HomePage;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
describe('Protractor Login checing ',() => {
  it('should add one and two', () => {
    const userNameField = browser.driver.findElement(by.id('emailuser'));
    const  userPassField = browser.driver.findElement(by.id('passworduser'));
   //  var userLoginBtn  = browser.driver.findElement(By.id('loginbtn'));
   // Fill input fields
   userNameField.sendKeys('prueba@email.com');
   userPassField.sendKeys('123456');
   // Ensure fields contain what we've entered
  expect(userNameField.getAttribute('value')).toEqual('nick@email.com');
   expect(userPassField.getAttribute('value')).toEqual('123456');
  });
});

