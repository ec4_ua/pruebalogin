/* eslint-disable @typescript-eslint/naming-convention */
import { Animation, AnimationController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage  {
  @ViewChild('loadimg',{read: ElementRef}) loadimg: ElementRef;
  formGroup: FormGroup;
  passwordControl = false;
  emailControl = false;
  ionViewWillEnter() {
    this.start();
  }

  validation_messages = {
    email: [
      { type: 'required', message: 'Email es requerido.' },
      { type: 'pattern', message: 'Porfavor inserte un email válido.' }
    ],
    password: [
      { type: 'required', message: 'Contraseña es requerida.' },
      { type: 'minlength', message: 'Contraseña debe tener al menos 5 caracteres.' },
    ],
  };
  constructor(public formBuilder: FormBuilder, private animationCtrl: AnimationController) {
    this.formGroup = this.formBuilder.group({

      email: [
        '',
        Validators.compose([
          Validators.minLength(4),
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
          Validators.required
        ])
      ],
      password: [
        '',
        Validators.compose([
          Validators.minLength(5),
          Validators.pattern('[0-9a-z-A-Z@.#*$!?&+-/]*'),
          Validators.required
        ])
      ]
    });
  }

  start(){
  const animation = this.animationCtrl.create('loading-animation')
  .addElement(this.loadimg.nativeElement)
  .duration(400)
  .direction('alternate')
  .iterations(5)
  .keyframes([
    { offset: 0, transform: 'scale(1)', opacity: '1' },
    { offset: 1, transform: 'scale(1.5)', opacity: '0.5'
 }
  ]);

animation.play();
  }
  onSubmit(formData: any) {
   if (!this.formGroup.valid){
    console.log('error');
    console.log(this.formGroup.get('password').invalid);
    console.log(this.formGroup.get('email').invalid);
      if (this.formGroup.get('password').invalid){
          this.passwordControl = true;
      }else{
        this.passwordControl = false;
      }

      if (this.formGroup.get('email').invalid){
        this.emailControl = true;
      }else{
        this.emailControl = false;
      }
   }
   else
    {console.log('ok');}
    // todo do something with our data like:
    // this.service.set(formData);
  }
}


